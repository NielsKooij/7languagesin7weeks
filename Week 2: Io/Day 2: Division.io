OperatorTable addOperator("/", 2)

Number / := method(
  x,
  if(x == 0, return 0, self / x)
)

4 / 0 println
