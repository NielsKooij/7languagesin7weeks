addTwoDim := method(
  arr,
  total := 0
  for(i, 1, arr size, total = total + (arr at(i - 1) sum))
  return total
)

List myAverage := method(
  self sum / self size
)

List mySafeAverage := method(
  size := self size
  for(i, 1, size, if(self at(i - 1) type != Number type, Exception raise("Not all elements are number"), continue))
  if(size == 0,
    return 0,
    return self sum / self size
  )
)

l := list(list(1,2,3), list(4,5,6), list(7,8,9))
"Adding all elements of: list(list(1,2,3), list(4,5,6), list(7,8,9))" println
addTwoDim(l) println

"Average of: list(1,2,3)" println
l at(0) myAverage println

"Safe average of: list(1,2,3)" println
l at(0) mySafeAverage println

"Safe average of empty list" println
list() mySafeAverage println

"Safe average of list with not only numbers" println
list(1,2,3,"a") mySafeAverage println
