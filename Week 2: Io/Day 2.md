## Fibonacci
> Write a program to find the nth Fibonacci number. As a bonus, solve the problem with recursion and with loops.

See `Day 2: Fibonacci.io`

## Division
> How would you change / to return 0 if the denominator is zero?

See `Day 2: Division.io`

## Sum
> Write a program to add up all of the numbers in a two-dimensional array.

See `Day 2: List.io` with method `addTwoDim`

## myAverage
> Add a slot called myAverage to a list that computes the average of all the numbers in a list. What happens if there are no numbers in a list? (Bonus: Raise an Io exception if any item in the list is not a number)

See `Day 2: List.io` with method `List myAverage` <br/>
`list() sum` evaluates to `nil` which produces an error in the function. <br/>
I created the method `List mySafeAverage` that will raise an error and handle empty lists.

## Matrix
> Write a prototype for a two-dimensional list. The dim(x, y) method should allocate a list of y lists that are x elements long. set(x,y,value) should set a value, and get(x,y) should return that value.

> Bonus: Write a transpose method so that (new_matrix get(y,x) == matrix get(x,y)) on the original list.

> Write the matrix to a file, and read the matrix from a file.

See `Day 2: Matrix.io`

## Guessing game
> Write a program that gives you ten tries to guess a random number from 1-100. If you would like, give a hint of "hotter" or "colder" after the first guess.

See `Day 2: Guessing.io`
