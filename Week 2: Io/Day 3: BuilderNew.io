Builder html(
  head(
    title("Io HtmlDSL")
  ),
  body(
    div(
      {"class": "container", "id": "app"},
      p("Languages"),
      ul(
        li("Io"),
        li("Lua"),
        li("Javascript")
      )
    )
  )
)
