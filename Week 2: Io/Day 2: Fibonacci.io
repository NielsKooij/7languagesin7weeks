fib := method(
  n,
  x := 0
  y := 1
  if (n == 0 or n == 1,
    n,
    (n - 1) repeat (z := x; x = y; y = z + y)
  )
)

"Loop: " println
for(i, 1, 10, fib(i) println)

fib_rec := method(
  n,
  go(n, 0, 1)
)

# Using tail recursion for better performance
go := method(
  n,
  a,
  b,
  if(n == 0, a,
    if(n == 1, b,
      go((n - 1), b, a + b)
    )
  )
)

"Recursive: " println
for(i, 1, 10, fib_rec(i) println)
