OperatorTable addAssignOperator(":", "atPutValue")

Builder := Object clone do(
  indentStack := 0

  forward := method(
    addIndent

    args := call message arguments

    write("<", call message name)
    if(args size > 0 and args at(0) name == "curlyBrackets",
      doMessage(args at(0)) foreach(key, value,
        write(" ", key, "=\"", value, "\"")
      )
      args remove(0)
    )
    writeln(">")

    args foreach(
      arg,
      indentStack = indentStack + 1
      content := self doMessage(arg)
      
      if(content type == "Sequence",
        addIndent
        writeln(content)
      )
      indentStack = indentStack - 1
    )

    addIndent
    writeln("</", call message name, ">"))

  curlyBrackets := method(
    r := Map clone
    call message arguments foreach(arg,
      r doMessage(arg)
      )
    r
  )

  Map atPutValue := method(
    self atPut(
      call evalArgAt(0) asMutable removePrefix("\"") removeSuffix("\""),
      call evalArgAt(1)
    )
  )

  addIndent := method(
    indentStack repeat(write("  "))
  )
)

s := File with("Day 3: BuilderNew.io") openForReading contents
doString(s)
