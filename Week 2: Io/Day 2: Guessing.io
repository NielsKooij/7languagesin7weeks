getNumber := method(
  number := "NaN" asNumber

  while(number isNan,
    File standardOutput write("Enter a number: ")
    number := File standardInput readLine asNumber

    if(number isNan,
      File standardOutput write("That is not a number\n"),
      return number
    )
  )
)

guessingGame := method(
  value := (Random value * 100) floor
  lastGuess := nil

  "Guess a number from 0-100" println
  for(i, 1, 10,
    guess := getNumber

    if(guess == value,
      "You guessed correctly" println return,
      if(lastGuess == nil,
        lastGuess = guess,
        if(abs(value - guess) == abs(value - lastGuess),
          "Same temperature as the last guess." println,
          if(abs(value - guess) < abs(value - lastGuess),
            "Hotter than " print
            lastGuess println
            lastGuess = guess,
            "Colder than " print
            lastGuess println
          )
        )
      )
    )
  )
)

difference := method(a, b, abs(a - b))

abs := method(x, if(x < 0, return x * -1, return x))

guessingGame
