## Example problems
> Find some Io example problems

https://gist.github.com/jezen/7972975

## Io Community
> Find an Io community that will answer questions

I was unable to find a forum like community for Io. Some "communities" I did find are
- https://github.com/IoLanguage/io
- https://iolanguage.org/links.html
- https://twitter.com/iolanguage

## Style guide with Io idioms
> Find a style guide with Io idioms

https://en.wikibooks.org/wiki/Io_Programming/Io_Style_Guide

## Strongly or weakly typed?
> Is Io strongly typed or weakly typed? Support your answer with code.

```
Io> 1+1
==> 2

Io> 1 + "one"

  Exception: argument 0 to method '+' must be a Number, not a 'Sequence'
  ---------
  message '+' in 'Command Line' on line 1
```
The language is strongly typed as a Number cannot be added to a String

## True or false
> Is 0 true or false? What about the empty string? Is nil true or false? Support your answer with code.

```
Io> true and 0
==> true

Io> true and ""
==> true

Io> true and nil
==> false
```

## Slots on prototype
> How can you tell what slots a prototype supports?

```
Io> Object slotNames
==> list(or, coroDo, ifError...)

Io> Object proto
==>  Object_0x1130710:
  Lobby            = Object_0x1130710
  Protos           = Object_0x11304b0
  _                = Object_0x1130710
  exit             = method(...)
  forward          = method(...)
  set_             = method(...)
```

## Different equals signs
> What is the difference between = (equals), := (colon equals), and ::= (colon colon equals)? When would you use each one?

https://iolanguage.org/guide/guide.html#Syntax-Assignment

Io has three assignment operators:

| operator | action                                                         |
|----------|----------------------------------------------------------------|
| ::=      | Creates slot, creates setter, assigns value                    |
| :=       | Creates slot, assigns value                                    |
| =        | Assigns value to slot if it exists, otherwise raises exception |
