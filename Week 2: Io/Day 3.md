## Xml Indentation
> Enhance the Xml program to add spaces to show the indentation structure.

See `Day 3: xml.io`

## List
> Create a list syntax that uses brackets

See `Day 3: List.io`.
`call message arguments` returns an object that contains all the arguments as a list. In this case that is exactly what we want to return.

## Xml attributes
> Enhance the Xml program to handle attributes: if the first argument os a map (use curly brackets syntax), add attributes to the Xml program. For example: `book({"author": "Tate"}...)` would print `<book author="Tate">`

See `Day 3: xml.io`
