ListTwoDim := List clone

ListTwoDim dim := method(
  x,
  y,
  self x := x
  self y := y
  for(i, 1, y,
    l := list()
    x repeat (l append(0))
    self append(l)
  )
)

ListTwoDim set := method(
  x,
  y,
  value,
  self at(y) atPut(x, value)
  self
)

ListTwoDim get := method(
  x,
  y,
  self at(y) at(x)
)

ListTwoDim transpose := method(
  l := ListTwoDim clone dim(self y, self x)
  for(y, 1, self y,
    for(x, 1, self x,
      l at(x - 1) atPut(y - 1, self at(y - 1) at(x - 1))
    )
  )
  return l
)

ListTwoDim toFile := method(
  fileName,
  file := File clone openForUpdating(fileName)
  for(y, 1, self y,
    for(x, 1, self x,
      file write(self get(x - 1, y - 1) asString)
      file write(" ")
    )
    file write("\n")
  )
  file close
)

ListTwoDim fromFile := method(
  fileName,
  file := File clone openForUpdating(fileName)
  lines := file readLines

  for(y, 1, lines size,
    l := lines at(y - 1) split
    self append(l)
    self x := l size
  )
  self y := lines size
  file close

  return self
)

l := ListTwoDim clone dim(3, 4)

"3 by 4 list:" println
l println

"" println
"Set 1,0 to 10: " println
l set(1, 0, 10)
l println

"" println
"Get 1,0:" println
l get(1, 0) println

"" println
"Transpose: " println
l2 := l transpose
l2 println

(l get(1,0) == l2 get(0,1)) println

"" println
"To file: " println
l println
l toFile("matrix.txt")

"From file: " println
l1 := ListTwoDim clone
l1 fromFile("matrix.txt")
l1 println
