## Clojure sequences
> Find examples using Clojure sequences.

https://riptutorial.com/clojure/example/4528/sequences

> Find the formal definition of a Clojure function.

https://clojure.org/guides/learn/functions

## Big
> Implement a function called `(big st n)` that returns true if a string st is longer than n characters.

```
user=> (defn big
             "Returns true if the length of st if larger than n characters"
             [st, n]
             (> (count st) n))
#'user/big
user=> (big "SomeString", 3)
true
user=> (big "SomeString", 15)
false
```

## Collection type
> Write a function called `(collection-type col)` that returns :list, :map, or :vector based on the type of collection col.

```
user=> (defn collection-type
  #_=>     "Returns the type of the collection"
  #_=>     [col]
  #_=>     (type col)
  #_=>     (let [colType (str (type col))]
  #_=>       (case colType
  #_=>             "class clojure.lang.PersistentList" :list
  #_=>             "class clojure.lang.PersistentVector" :vector
  #_=>             "class clojure.lang.PersistentHashSet" :set
  #_=>             "class clojure.lang.PersistentArrayMap" :map
  #_=>             :unkown_collection
  #_=>             ))
  #_=>     )
#'user/collection-type
user=> (collection-type (list 1 2 3))
:list
user=> (collection-type [:hutt :wookie :ewok])
:vector
user=> (collection-type #{:x-wing :y-wing :tie-fighter})
:set
user=> (collection-type {:chewie :wookie, :lea :human})
:map
```
