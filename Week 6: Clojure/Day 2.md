## Common macros
> Find the implementation of some of the commonly used macros in the Clojure language.

https://clojure.org/reference/macros

## Lazy sequence
> Find an example of defining your own lazy sequence.

https://clojuredocs.org/clojure.core/lazy-seq

## Defrecord and Protocol
> Find the current status of the `defrecord` and `protocol` features.

`defrecord`: https://clojure.org/reference/datatypes
<br>
`protocol`: https://clojure.org/reference/protocols

## Unless else
> Implement an unless with an else condition using macros.

```
user=> (defmacro unless [test body elseBody]
  #_=>   (list 'if (list 'not test) body elseBody))
#'user/unless

user=> (macroexpand '(unless condition body elsebody))
(if (not condition) body elsebody)

user=> (unless true (println "ifBody") (println "elseBody"))
elseBody
nil

user=> (unless false (println "ifBody") (println "elseBody"))
ifBody
nil
```

## Defrecords and Protocol implementation
> Write a type using `defrecord` that implements a `protocol`.

```
user=> (defprotocol Animal
  #_=>   "A simple animal abstraction"
  #_=>   (talk [a])
  #_=> )
Animal

user=> (defrecord Cat [name]
  #_=>   Animal
  #_=>   (talk [_] (println "meow"))
  #_=>   )
user.Cat

user=> (def c (Cat. ["Some Cat Name"]))
#'user/c

user=> (talk c)
meow
nil
```
