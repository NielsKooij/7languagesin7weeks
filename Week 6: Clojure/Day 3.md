## Concurrent Queue
> Find a queue implementation that blocks when the queue is empty and waits for a new item in the queue.

https://clojuredocs.org/clojure.core/seque

## Accounts
> Use refs to create a vector of accounts in memory. Create debit and credit functions to change the balance of an account.

I used atoms instead of refs. <br>
See `Day 3: Account.clj` for the solution. Use as following:

```
user=> (def accs (create))
#'user/accs

user=> (newAcc accs :Acc1)
{:Acc1 0.0}

user=> (newAcc accs :Acc2)
{:Acc1 0.0, :Acc2 0.0}

user=> (credit accs :Acc1 100)
{:Acc1 100.0, :Acc2 0.0}

user=> (credit accs :Acc2 75)
{:Acc1 100.0, :Acc2 75.0}

user=> (debit accs :Acc1 15)
{:Acc1 85.0, :Acc2 75.0}

user=> (balance accs :Acc1)
85.0

user=> @accs
{:Acc1 85.0, :Acc2 75.0}
```
