(defn create
  []
  (atom {})
  )

(defn balance
  [accounts name]
  (@accounts name)
  )

(defn put
  ([accounts value-map]
    (swap! accounts merge value-map))
  ([accounts key value]
    (swap! accounts assoc key value))
  )

(defn newAcc
  [accounts name]
  (put accounts name 0.0))

(defn credit
  [accounts name amount]
  (swap! accounts update name + amount)
  )

(defn debit
  [accounts name amount]
  (swap! accounts update name - amount)
  )
