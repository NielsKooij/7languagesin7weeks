;; unless with else
(defmacro unless [test body elseBody]
  (list 'if (list 'not test) body elseBody))

;; Some random protocol. This is more inheritance than an interface but you get the point.
(defprotocol Animal
  "A simple animal abstraction"
  (talk [a])
)

(defrecord Cat [name]
  Animal
  (talk [_] (println "meow"))
  )
