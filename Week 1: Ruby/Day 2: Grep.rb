def grep_file(filePath, regex)
  i = 0
  File.open(filePath, "r") do |f|
    f.each_line do |line|
      i = i + 1
      puts "Line #{i}: #{line}" if regex.match(line)
    end
  end
end

# Finds all questions in "Day 2.md"
grep_file("Day 2.md", /\?/)
