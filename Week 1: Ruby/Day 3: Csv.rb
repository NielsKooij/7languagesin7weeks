module ActsAsCsv
  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    def acts_as_csv
      include InstanceMethods
    end
  end

  module InstanceMethods
    include Enumerable

    def read
      @csv_contents = []
      filename = self.class.to_s.downcase + '.txt'
      file = File.new(filename)
      @headers = file.gets.chomp.split(', ')

      file.each do |row|
        @csv_contents << row.chomp.split(', ')
      end
    end

    def each
      return enum_for(:each) unless block_given?
      @csv_contents.each { |i| yield Row.new(headers, i)}
    end

    attr_accessor :headers, :csv_contents
    def initialize
      read
    end
  end
end

class Row < Array
  attr_accessor :headers

  def initialize headers, row
    @headers = headers
    super(row)
  end

  def method_missing name, *args
    index = headers.index name.to_s
    return self[index] if index
    return nil
  end
end

class RubyCsv
  include ActsAsCsv
  acts_as_csv
end

csv = RubyCsv.new
csv.each { |row| puts row.one}
