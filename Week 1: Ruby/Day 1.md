## The ruby core API
https://ruby-doc.org/core-2.7.1/

## Programming Ruby: The Pragmatic Programmer's Guide
https://ruby-doc.com/docs/ProgrammingRuby/

## String substitution
https://ruby-doc.org/core-2.7.1/String.html#method-i-gsub
```
"hello".gsub(/[aeiou]/, '*')                  #=> "h*ll*"
"hello".gsub(/([aeiou])/, '<\1>')             #=> "h<e>ll<o>"
"hello".gsub(/./) {|s| s.ord.to_s + ' '}      #=> "104 101 108 108 111 "
"hello".gsub(/(?<foo>[aeiou])/, '{\k<foo>}')  #=> "h{e}ll{o}"
'hello'.gsub(/[eo]/, 'e' => 3, 'o' => '*')    #=> "h3ll*"
```

## Regular expressions
https://ruby-doc.org/core-2.7.1/Regexp.html

## Ranges
https://ruby-doc.org/core-2.5.1/Range.html
