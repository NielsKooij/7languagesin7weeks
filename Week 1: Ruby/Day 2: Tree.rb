class Tree
  attr_accessor :children, :node_name

  def initialize(name, children = {})
    @node_name = name
    @children = children.collect {|k, v| Tree.new(k, v) }
  end

  def visit_all(&block)
    visit &block
    children.each {|c| c.visit_all &block}
  end

  def visit(&block)
    block.call self
  end
end

hash_tree = {'grandpa' => {'dad' => {'child1' => {}, 'child2' => {} }, 'uncle' => {'child3' => {}, 'child4' => {} } } }
root = hash_tree.keys[0]
tree = Tree.new(root, hash_tree[root])

tree.visit_all{ |n| puts n.node_name }
