## File access
> Find out how to access files with and without code blocks. What is the benefit of the code block?

From https://stackoverflow.com/questions/5545068/what-are-all-the-common-ways-to-read-a-file-in-ruby
```
File.open("my/file/path", "r") do |f|
  f.each_line do |line|
    puts line
  end
```
vs
```
f = File.open("my/file/path", "r")
f.each_line do |line|
  puts line
end
f.close
```
When using the code block the file closes automatically. It is also more expressive in my opinion.

## Hash - array translation
> How would you translate a hash to an array? Can you translate arrays to hashes?

### Hash => Array
When flattening a hash you will lose information about the keys.
```
{1 => 'one', 2 => 'two', 3 => 'three'}
  .inject([]) { |memo, (key, value)| memo.push(value) }
```
You could solve this by using a tuple (or a list of two elements) to keep this information.
```
{1 => 'one', 2 => 'two', 3 => 'three'}
  .inject([]) { |memo, entry)| memo.push(entry) }
```
You could also alternate between keys and values.
```
{1 => 'one', 2 => 'two', 3 => 'three'}
  .inject([]) do |memo, (key, value)|
  memo.push(key)
  memo.push(value)
end
```

### Array => Hash
Because you do not have any key information you have to generate keys to turn an array into a hash. As long as you can generate a unique key for each element this will work. For instance the index. This is the example I came up with but there must be an easier way.
```
('a'..'z').to_a.inject({}) do |memo, elem|
  index = memo.max_by{|k, v| k}
  if index == nil
    index = 0
  else
    index = index[0] + 1
  end
  memo[index] = elem
  memo
end
```

## Hash iteration
> Can you iterate through a hash?

Yes this is possible with the `each` code block.
```
{1 => 'one', 2 => 'two', 3 => 'three'}.each { |k, v| puts "#{k} => #{v}"}
```

## Array datastructures
> You can use Ruby arrays as stacks. What other common data structures do arrays support?

- Stack
- Queue
- Linked list
- Set

## Print in sets of 4
### each
```
(1..16).to_a.each do |i|
  print i.to_s + " "
  puts "" if i % 4 == 0
end
```
### each_slice
```
(1..16).to_a.each_slice(4) do |s|
  s.each{ |i| print i.to_s + " "}
  puts ""
end
```
A shorter version that prints the array instead of each individual element:
```
(1..16).to_a.each_slice(4) { |i| p i}
```
