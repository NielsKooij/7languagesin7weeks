def hello_world
  puts "Hello, world."
end

def index_ruby
  index = 'Hello, Ruby'.index('Ruby')
  puts "index  #{index}"
end

def print_ten
  i = 0
  while i < 10
    puts "Niels Kooij"
    i += 1
  end
end

def print_ten_with_variable
  i = 1
  while i <= 10
    puts "This is sentence #{i}"
    i += 1
  end
end

def guessing_game
  number = rand(10)
  puts "Guess a number from 0 to 9"

  loop do
    guess = gets
    guess = guess.to_i

    puts "Too high" if guess > number
    puts "Too low" if guess < number
    break if guess == number

    puts "guess again"
  end
  puts "You guessed correctly"
end

hello_world
puts ""

index_ruby
puts ""

print_ten
puts ""

print_ten_with_variable
puts ""

guessing_game
puts ""
