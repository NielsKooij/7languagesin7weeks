-module(day_1).
-export([words/1]).
-export([count/1]).
-export([match/1]).

words([]) -> 0;
words([H|T]) -> 1 + words(H, T).

words(_, []) -> 0;
words(Last, [H|T]) ->
  if
    [Last] == " " ->
      1 + words(H, T);
    true ->
      0 + words(H, T)
  end.

count(N) -> count(0, N).

count(N, N) -> [];
count(I, N) -> [I] ++ count(I + 1, N).

match(success) -> io:fwrite("success~n");
match({error, Message}) -> io:fwrite("error: " ++ Message ++ "~n").
