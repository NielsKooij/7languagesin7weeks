## Erlang documentation
> Find the erlang language's official site.

https://www.erlang.org/

> Find the documentation for Erlang's libraries

https://erlang.org/doc/search/

## Words in String
> Write a function that uses recursion to return the number of words in a string.

See `day_1.erl` for the solution. Use as following:
```
1> day_1:words("This is a string containing words").
6
```

## Count
> Write a function that uses recursion to count to ten.

See `day_1.erl` for the solution. Use as following:
```
1> day_1:count(10).
[0,1,2,3,4,5,6,7,8,9]
```

## Matching
> Write a function that uses matching to selectively print "success" or "error: message" given input of the form `{error, message}` or `success`.

See `day_1.erl` for the solution. Use as following:
```
1> day_1:match(success).                
success
ok
2> day_1:match({error, "ErrorMessage"}).
error: ErrorMessage
ok
```
