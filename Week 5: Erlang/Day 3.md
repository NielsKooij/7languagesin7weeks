## OTP Server
> Find an OTP service that will restart a process when it dies.

Documentation for the `supervisor`: http://erlang.org/doc/man/supervisor.html
<br>
Design principles on how to implement a `supervisor`: https://erlang.org/doc/design_principles/sup_princ.html

> Find documentation for building a simple OTP server.

https://erlang.org/doc/design_principles/gen_server_concepts.html

## Monitoring
> Monitor the translate_service and restart it should it die.

See `Day 3/translation_service.erl` and `Day 3/translation_monitor.erl` for the solution. Use as following:
```
1> c(translation_monitor).
{ok,translation_monitor}
2> c(translation_service).
{ok,translation_service}
3> Monitor = spawn(fun translation_monitor:loop/0).
<0.91.0>
4> Monitor ! new.
Creating and monitoring process.
new
5> translation_service:translate(translator, "casa").
"house"
6> translator ! terminate.
Terminating...terminate
Process <0.93.0> terminated with reason "terminate call".
Restarting...
Creating and monitoring process.
7> translation_service:translate(translator, "blanca").
"white"
8>
```

> Make a monitor for the Doctor monitor. If either monitor dies, restart it.

See `Day 3/selfhealer.erl` for the solution. Use as following:
```
1> c(selfhealer).
{ok,selfhealer}
2> Healer = spawn(fun selfhealer:loop/0).
<0.85.0>
3> Healer ! new.
[<0.85.0>]: Created instance <0.87.0> to keep me alive.
new
[<0.87.0>]: Monitoring process <0.85.0>.
4> exit(Healer, kill).
[<0.87.0>]: Process <0.85.0> terminated with reason killed.
true
[<0.87.0>]: Restarting...
[<0.87.0>]: Created instance <0.89.0> to keep me alive.
[<0.89.0>]: Monitoring process <0.87.0>.
5> exit(<0.89.0>, kill).
[<0.87.0>]: Process <0.89.0> terminated with reason killed.
true
[<0.87.0>]: Restarting...
[<0.87.0>]: Created instance <0.91.0> to keep me alive.
[<0.91.0>]: Monitoring process <0.87.0>.
6>
```
