-module(day_2).
-export([keyMap/2]).
-export([shoppingList/1]).
-export([ticTacToe/1]).

keyMap(Search, List) ->
  lists:map(
    fun({_, Value}) -> Value end,
    lists:filter(
      fun({Key, _}) -> Key == Search end,
      List
    )
  ).

shoppingList(List) -> [{Item, Quantity * Price} || {Item, Quantity, Price} <- List].


% Tic-tac-toe

ticTacToe(Board) ->
  Player1Wins = checkAll(1, Board),
  Player2Wins = checkAll(2, Board),
  FullBoard   = isFull(Board),
  if
    Player1Wins -> x;
    Player2Wins -> o;
    FullBoard   -> cat;
    true        -> no_winner
  end.

checkAll(Player, Board) ->
  WinningRows =
  [
    [0,1,2], [3,4,5], [6,7,8],
    [0,3,6], [1,4,7], [2,5,8],
    [0,4,8], [2,4,6]
  ],
  lists:any(
    fun(Bool) -> Bool end,
    lists:map(
      fun([A,B,C]) -> checkRow(Player, [lists:nth(A + 1, Board), lists:nth(B + 1, Board), lists:nth(C + 1, Board)]) end,
      WinningRows)).

checkRow(Player, Row) -> lists:all(fun(Elem) -> Elem == Player end, Row).

isFull([]) -> true;
isFull([1|T]) -> isFull(T);
isFull([2|T]) -> isFull(T);
isFull([_|_]) -> false.
