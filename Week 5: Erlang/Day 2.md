## Map
> Consider a list of keyword-value tuples, such as `[{erlang, "a functional language"}, {ruby, "an OO language"}]`. Write a function that accepts the list and a keyword and returns the associated value for the keyword.

See `day_2.erl` for the solution. Use as following:
```
1> day_2:keyMap(erlang, [{erlang, "a functional language"}, {ruby, "an OO language"}]).
["a functional language"]
```
This will return all values from tuples with key `erlang`. To get the first element you can use `lists:nth(1, Result)`.

## Shopping list
> Consider as shopping list that looks like `[{item, quantity, price}, ...]`. Write a list comprehension that builds a list of items of the form `[{item, total_price}, ...]`, where total_price is quantity times price.

See `day_2.erl` for the solution. Use as following:
```
1> day_2:shoppingList([{"Bread", 2, 1.05}, {"Cheese", 1, 5.0}]).
[{"Bread",2.1},{"Cheese",5.0}]
```

## TicTacToe
> Write a program that reads a tic-tac-toe board presented as a list or a tuple of size nine. Return the winner (`x` or `o`) if a winner has been determined, `cat` if there are no more possible moves, or `no_winner` if no player has won yet.

See `day_2.erl` for the solution. I changed `x` and `o` to `1` and `2` because it was easier to check rows. You could write a map function that converts these lists.
<br><br>
Use as following:
```
1> c(day_2).
{ok,day_2}
2> day_2:ticTacToe([1,2,1,2,2,1,1,1,2]).
cat
3> day_2:ticTacToe([1,2,0,2,2,0,1,1,1]).
x
4> day_2:ticTacToe([1,1,0,2,2,2,1,0,0]).
o
5> day_2:ticTacToe([1,1,0,2,2,0,1,0,0]).
no_winner
```
