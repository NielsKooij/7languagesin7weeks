-module(selfhealer).
-export([loop/0]).

loop() ->
    process_flag(trap_exit, true),
    receive
        new ->
            Healer = spawn(fun selfhealer:loop/0),
            link(Healer),
            io:format("[~p]: Created instance ~p to keep me alive.~n", [self(), Healer]),
            Healer ! {monitor, self()},
            loop();

        {monitor, Process} ->
            io:format("[~p]: Monitoring process ~p.~n", [self(), Process]),
            link(Process),
            loop();

        {'EXIT', From, Reason} ->
            io:format("[~p]: Process ~p terminated with reason ~p.~n", [self(), From, Reason]),
            io:format("[~p]: Restarting...~n", [self()]),
            self() ! new,
            loop()
    end.
