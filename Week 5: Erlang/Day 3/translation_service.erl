-module(translation_service).

-export([loop/0]).
-export([translate/2]).

loop() ->
    receive

        {From, "casa"} ->
            From ! "house",
            loop();

        {From, "blanca"} ->
            From ! "white",
            loop();

        {From, _} ->
            From ! "I don't understand.",
            loop();

        terminate ->
            io:format("Terminating..."),
            exit(self(), "terminate call")
    end.

translate(To, Word) ->
    To ! {self(), Word},
    receive
      Translation -> Translation
    end.
