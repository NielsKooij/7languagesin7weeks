## Elm documentation
> How do you compile an Elm program?

https://guide.elm-lang.org/install/elm.html

> Where would you go for Elm support?

https://elm-lang.org/community

## Easy functions
> Write a function to find the product of a list of numbers.

```
> product = List.foldr (*) 1
<function> : List number -> number
> product [1,2,3,4,5,6,7,8,9,10]
3628800 : number
>
```

---
> Write a function to return all of the x fields from a list of point records.

```
> xFields = List.map (.x)
<function> : List { a | x : b } -> List b
> points = [{x=0,y=0}, {x=1,y=1}, {x=2,y=2}]
[{ x = 0, y = 0 },{ x = 1, y = 1 },{ x = 2, y = 2 }]
    : List { x : number, y : number1 }
> xFields points
[0,1,2] : List number
>
```

---
> Use records to describe a person containing name, age and address. You should also express address as a record.

```
> type alias Address = { street : String, city : String, zipCode : String }
> type alias Person = { name : String, age : Int, address : Address }
> Person "Firstname" 25 <| Address "Randomstreet" "Randomcity" "Randomzipcode"
{ address = { city = "Randomcity", street = "Randomstreet", zipCode = "Randomzipcode" }, age = 25, name = "Firstname" }
    : Person
```

---
> Is it easier to use abstract data types or records to solve the previous problem? Why?

```
> type Address = A String String String
> type Person = P String Int Address
> P "Firstname" 25 <| A "Randomcity" "RandomStreet" "Randomzipcode"
P "Firstname" 25 (A "Randomcity" "RandomStreet" "Randomzipcode")
    : Person
```

Creating the type is just as easy but using it will be harder. To get any information out you will need to write your own functions. For instance to get the name of a person we could write the following function `name (P n _ _) = n`. Record types express the concept of a person better. You know it contains a name, age and address. With abstract data types this is not immediately clear.

## Medium functions
> Write a function called multiply.

```
> multiply x y = x * y
<function> : number -> number -> number
```

---
> Use currying to express 6 * 8.

```
> (multiply 6) 8
48 : number
```

---
> Make a list of person records. Write a function to find all of the people in your list older than 16.

I will use the same person record as before.
```
older : List Person -> List Person \
older = List.filter (\p -> p.age > 16)

p1 = Person "Firstname" 25 <| Address "Randomstreet" "Randomcity" "Randomzipcode"
p2 = Person "Firstname" 15 <| Address "Randomstreet" "Randomcity" "Randomzipcode"
people = [p1, p2]

> older people
[{ address = { city = "Randomcity", street = "Randomstreet", zipCode = "Randomzipcode" }, age = 25, name = "Firstname" }]
    : List { address : Address, age : Int, name : String }
```

## Functions hard
> Write the same function, but allow records where the age field might be noting. How does Elm support nil values?

Elm uses the Maybe type to represent nil values just like haskell. `type Maybe a = Just a | Nothing` where `Nothing` represents a nil value.

```
type alias Address = { street : String, city : String, zipCode : String }
type alias Person = { name : String, age : Maybe Int, address : Address }

older : List Person -> List Person \
older = List.filter (\p -> case p.age of \
    Nothing  -> False \
    Just age -> age > 16)

p1 = Person "Firstname" (Just 25) <| Address "Randomstreet" "Randomcity" "Randomzipcode"
p2 = Person "Firstname" (Just 15) <| Address "Randomstreet" "Randomcity" "Randomzipcode"
p3 = Person "Firstname" Nothing <| Address "Randomstreet" "Randomcity" "Randomzipcode"
people = [p1, p2, p3]

> older people
[{ address = { city = "Randomcity", street = "Randomstreet", zipCode = "Randomzipcode" }, age = Just 25, name = "Firstname" }]
```
