## Sudoku
> Modify the Sudoku solver to work on six-by-six puzzles (squares are 2x3) and 9x9 puzzles.

> Make the Sudoku solver print prettier solutions.

See `Day 3: Sudoku.pl` for the solutions.
<br><br>
I only did the 9x9 since this approach is the same for both.

```
?- sudoku([5,3,_,_,7,_,_,_,_,
           6,_,_,1,9,5,_,_,_,
           _,9,8,_,_,_,_,6,_,
           8,_,_,_,6,_,_,_,3,
           4,_,_,8,_,3,_,_,1,
           7,_,_,_,2,_,_,_,6,
           _,6,_,_,_,_,2,8,_,
           _,_,_,4,1,9,_,_,5,
           _,_,_,_,8,_,_,7,9], Solution).

|5|3|4|6|7|8|9|1|2|
|6|7|2|1|9|5|3|4|8|
|1|9|8|3|4|2|5|6|7|
|8|5|9|7|6|1|4|2|3|
|4|2|6|8|5|3|7|9|1|
|7|1|3|9|2|4|8|5|6|
|9|6|1|5|3|7|2|8|4|
|2|8|7|4|1|9|6|3|5|
|3|4|5|2|8|6|1|7|9|

Solution = [5, 3, 4, 6, 7, 8, 9, 1, 2|...].
```

I do not like this solution as it only works on one specific instance of this problem, the 9x9 sudoku. I wanted to find a solution that works for all sizes sudoku.
<br>
See `Day 3: SudokuGeneric.pl` for a more generic solution for sudoku puzzles. A few examples with differently shaped sudokus:

```
?- sudoku(3, 3, 9,
|          [5,3,_,_,7,_,_,_,_,
|           6,_,_,1,9,5,_,_,_,
|           _,9,8,_,_,_,_,6,_,
|           8,_,_,_,6,_,_,_,3,
|           4,_,_,8,_,3,_,_,1,
|           7,_,_,_,2,_,_,_,6,
|           _,6,_,_,_,_,2,8,_,
|           _,_,_,4,1,9,_,_,5,
|           _,_,_,_,8,_,_,7,9],
|           Solution).
|5|3|4|6|7|8|9|1|2|
|6|7|2|1|9|5|3|4|8|
|1|9|8|3|4|2|5|6|7|
|8|5|9|7|6|1|4|2|3|
|4|2|6|8|5|3|7|9|1|
|7|1|3|9|2|4|8|5|6|
|9|6|1|5|3|7|2|8|4|
|2|8|7|4|1|9|6|3|5|
|3|4|5|2|8|6|1|7|9|
Solution = [5, 3, 4, 6, 7, 8, 9, 1, 2|...] .

?- sudoku(2, 2, 4,
|          [1,_,_,_,
|           _,_,_,4,
|           _,_,2,_,
|           _,3,_,_],
|           Solution).
|1|4|3|2|
|3|2|1|4|
|4|1|2|3|
|2|3|4|1|
Solution = [1, 4, 3, 2, 3, 2, 1, 4, 4|...] .

?- sudoku(2, 3, 6,
|          [_,_,3,_,1,_,
|           5,6,_,3,2,_,
|           _,5,4,2,_,3,
|           2,_,6,4,5,_,
|           _,1,2,_,4,5,
|           _,4,_,1,_,_],
|           Solution).
|4|2|3|5|1|6|
|5|6|1|3|2|4|
|1|5|4|2|6|3|
|2|3|6|4|5|1|
|3|1|2|6|4|5|
|6|4|5|1|3|2|
Solution = [4, 2, 3, 5, 1, 6, 5, 6, 1|...]
```
