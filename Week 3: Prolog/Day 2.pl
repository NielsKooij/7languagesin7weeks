fib(0, 0).
fib(1, 1).
fib(A, N) :- N >= 0, N1 is N - 1, N2 is N - 2, fib(B, N1), fib(C, N2), A is B + C.

fib2(What, N) :- go(What, N, 0, 1).
go(A, 0, A, _).
go(B, 1, _, B).
go(What, N, A, B) :-
  N >= 0,
  N1 is N - 1,
  B1 is A + B,
  go(What, N1, B, B1).

factorial(1, 0).
factorial(1, 1).
factorial(What, N) :- N >= 0, N1 is N - 1, factorial(X, N1), What is X * N.

/* Reverse a list */
rev([], []).
rev([H|T], Reversed) :-
  rev(T, TRev),
  append(TRev, [H], Reversed).

/* Find the smallest element of a list */
min([Head|[]], Head).
min([Head|Tail], Min) :- min(Smallest, Tail), (Head < Smallest -> Min is Head; Min is Smallest).

/* Sort a list with mergesort */
mergeSort([], []).
mergeSort([Head|[]], [Head|[]]).
mergeSort(List, Sorted) :-
  div(A, B, List),
  mergeSort(SubSorted, A),
  mergeSort(SubSorted1, B),
  merge(SubSorted, SubSorted1, Sorted).

merge(Sorted, [], Sorted).
merge([], Sorted, Sorted).
merge([Head|Tail], [Head1|Tail1], Sorted) :-
  (Head < Head1 ->
    append([Head1], Tail1, List),
    merge(SubMerged, Tail, List),
    append([Head], SubMerged, Sorted);

    append([Head], Tail, List),
    merge(SubMerged, List, Tail1),
    append([Head1], SubMerged, Sorted)).

/* Divide a list in two lists of almost equal size */
div(A, B, List) :-
  append(A, B, List),
  length(A, N),
  length(B, N1),
  N2 is N - N1, (N2 = 0; N2 = 1).
