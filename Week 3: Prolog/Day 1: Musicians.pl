musician(brucedickinson).
musician(stevierayvaughan).
musician(davemustain).

genre(heavymetal, brucedickinson).
genre(blues, stevierayvaughan).
genre(trashmetal, davemustain).

instrument(vocals, brucedickinson).
instrument(guitar, stevierayvaughan).
instrument(vocals, stevierayvaughan).
instrument(guitar, davemustain).
instrument(vocals, davemustain).

musician_query(X, Y, Z) :- musician(X), genre(Y, X), instrument(Z, X).
