## Fibonacci and Factorial
> Find some implementations of a Fibonacci series and factorials. How do they work?

Before I went looking on the internet I wanted to try these implementations myself first. For the fibonacci series I came up with the following code.
```
fib(0, 0).
fib(1, 1).
fib(A, N) :- N >= 0, N1 is N - 1, N2 is N - 2, fib(B, N1), fib(C, N2), A is B + C.
```
This is a classic recursive implementation of the fibonacci series. The declaration of N1 and N2 is something I had to look up. `fib(B, N - 1)` didn't work. This implementation is very slow so I improved it using tail recursion to:
```
fib2(What, N) :- go(What, N, 0, 1).
go(A, 0, A, _).
go(B, 1, _, B).
go(What, N, A, B) :-
  N >= 0,
  N1 is N - 1,
  A1 is B,
  B1 is A + B,
  go(X, N1, A1, B1),
  What = X.
```

For factorial I made the following implementation:
```
factorial(1, 0).
factorial(1, 1).
factorial(What, N) :- N >= 0, N1 is N - 1, factorial(X, N1), What is X * N.
```

## Towers of Hanoi
> Find an implementation of the towers of Hanoi. How does it work?

I found an implementation and explanation here: https://www.cpp.edu/~jrfisher/www/prolog_tutorial/2_3.html

## Not expressions
> What are some of the problems when dealing with "not" expressions? Why do you have to be careful with negation in Prolog?

Prolog can only determine if a proposition is false by trying to prove it. If that attempt failes, it concludes the proposition is false. This may take infinite time in some cases. A full explanation can be found here: http://www.cse.unsw.edu.au/~billw/dictionaries/prolog/negation.html

## List recursion
> Reverse all elements of a list.

> Find the smallest element of a list.

> Sort the elements of a list.

See `Day 2.pl` for the solutions.
