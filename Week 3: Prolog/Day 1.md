## Prolog tutorials
> Find some free Prolog tutorials

- https://www.cpp.edu/~jrfisher/www/prolog_tutorial/pt_framer.html
- https://www.swi-prolog.org/pldoc/man?section=quickstart
- https://www.doc.gold.ac.uk/~mas02gw/prolog_tutorial/prologpages/
- http://www.cs.nuim.ie/~jpower/Courses/Previous/PROLOG/

## Support Forum
> Find a Prolog support forum.

- https://www.swi-prolog.org/forum

## Prolog reference
> Find an online reference for the prolog version you are using.

I am using `SWI-Prolog version 8.2.1 for x86_64-linux`. Information about this version can be found here: https://www.swi-prolog.org/pldoc/doc_for?object=manual

## Books
> Make a simple knowledge base. Represent some of your favorite books and authors.

See `Day 1: Books.pl` for the knowledge base.

> Find all books in your knowledge base written by one author.

```
?- written_by(Book, jonesbo).
Book = dezoon ;
Book = sneeuwman.
```

## Musicians
> Make a knowledge base representing musicians and there instruments. Also represent musicians and their genre of music.

See `Day 1: Musicians.pl` for the knowledge base.

> Find all musicians who play the guitar.

```
?- musician_query(X, Y, guitar).
X = stevierayvaughan,
Y = blues ;
X = davemustain,
Y = trashmetal ;
```
