book(dezoon).
book(sneeuwman).
book(baptismoffire).
book(bloodofelves).
book(sevenlanguagesinsevenweeks).

author(jonesbo).
author(andrzejsapkowski).
author(bruceatate).

written_by(dezoon, jonesbo).
written_by(sneeuwman, jonesbo).
written_by(baptismoffire, andrzejsapkowski).
written_by(bloodofelves, andrzejsapkowski).
written_by(sevenlanguagesinsevenweeks, bruceatate).
