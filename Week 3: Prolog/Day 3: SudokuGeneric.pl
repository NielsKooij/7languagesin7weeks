:- use_module(library(clpfd)).
:- use_module(library(lists)).

/* Width is the amount of sub-squares there are in the width of the sudoku.
   Height is the amount of sub-squares there are in the height of the sudoku.
   Dim is the dimension of the sudoku.

   For a traditional 9x9 sudoku: sudoku(3, 3, 9, Sudoku, Solution).
   For a 6x6 sudoku:             sudoku(2, 3, 6, Sudoku, Solution).
*/
sudoku(Width, Height, Dim, Puzzle, Solution) :-
  CellWidth is Dim / Width,
  CellHeight is Dim / Height,
  NrsPerSquare is CellWidth * CellHeight,
  Nrs is NrsPerSquare * NrsPerSquare,
  length(Puzzle, Nrs),

  Solution = Puzzle,
  valid_domain(Solution, 1, NrsPerSquare),

  /* Define rows */
  rows(Solution, NrsPerSquare, Rows),

  /* Define columns */
  cols(Rows, NrsPerSquare, Cols),

  /* Define squares */
  squares(Rows, Width, Height, CellWidth, CellHeight, Squares),

  /* Validate them all */
  valid(Rows),
  valid(Cols),
  valid(Squares),

  /* Print the solution */
  label(Solution),
  show(Rows).

/* Checks if every element in the list is unique.
*/
valid([]).
valid([H|T]) :-
  all_different(H),
  valid(T).

/* Checks if every element in the list is in a certain range.
*/
valid_domain(List, Min, Max) :-
  List ins Min..Max.

/* Prints the solution in a readable format.
*/
show([]).
show([H|T]) :- show_row(H), show(T).

show_row([]) :- write('|'), nl.
show_row([H|T]) :- write('|'), write(H), show_row(T).

/* Gets all the rows from a list.
   rows([1,2,3,4,5,6,7,8,9], 3, [[1,2,3], [4,5,6], [7,8,9]]).
*/
rows([], _, []).
rows(List, N, Rows) :-
  append(Head, Tail, List),
  length(Head, N),
  rows(Tail, N, Rest),
  append([Head], Rest, Rows).

/* Gets all the columns from the rows.
   cols([[1,2,3], [4,5,6], [7,8,9]], 3, [[1,4,7], [2,5,8], [3,6,9]]).
*/
cols(Rows, N, Cols) :-
  cols(Rows, 0, N, Cols).

cols(_, N, N, []).
cols(Rows, Index, N, [H|T]) :-
  Index1 is Index + 1,
  col(Rows, Index, H),
  cols(Rows, Index1, N, T).

col([], _, []).
col(Rows, Index, Col) :-
  maplist(nth0(Index), Rows, Col).

/* Gets all the squares.
*/
squares(Rows, Width, Height, CellWidth, CellHeight, Squares) :-
  N is Width * Height,
  squares(Rows, Width, CellWidth, CellHeight, 0, N, Squares).

squares(_, _, _, _, N, N, []).
squares(Rows, Width, CellWidth, CellHeight, Index, N, Squares) :-
  Index1 is Index + 1,
  square(Rows, Width, CellWidth, CellHeight, Index, Square),
  squares(Rows, Width, CellWidth, CellHeight, Index1, N, SubResult),
  append([Square], SubResult, Squares).

square(Rows, Width, CellWidth, CellHeight, N, Square) :-
  A is floor(N / Width) * CellHeight,
  B is (N mod Width) * CellWidth,
  drop(A, Rows, Rows1),
  take(CellHeight, Rows1, Rows2),
  maplist(drop(B), Rows2, Rows3),
  maplist(take(CellWidth), Rows3, Rows4),
  flatten(Rows4, Square).

drop(0, List, List).
drop(N, [_|T], Result) :-
  N1 is N - 1,
  drop(N1, T, Result).

take(0, _, []).
take(N, [H|T], Result) :-
  N1 is N - 1,
  take(N1, T, SubResult),
  append([H], SubResult, Result).
