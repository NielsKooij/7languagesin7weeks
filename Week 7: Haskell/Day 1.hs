module Main where
  main :: IO ()
  main = return ()

  allEven, allEven2, allEven3 :: [Integer] -> [Integer]

  allEven []     = []
  allEven (x:xs)
    | even x     = x : allEven xs
    | otherwise  = allEven xs

  allEven2 l  = filter even l

  allEven3 l  = [x | x <- l, even x]

  reverse' :: [a] -> [a]
  reverse' []     = []
  reverse' (x:xs) = reverse xs ++ [x]

  combination :: [([Char], [Char])]
  combination = [(x, y) | x <- colors, y <- colors, x < y]
    where colors = ["black", "white", "blue", "yellow", "red"]

  multiplication :: [(Integer, Integer, Integer)]
  multiplication = [(x, y, x * y) | x <- [1..12], y <- [1..12]]

  mapColoring :: [(([Char], [Char]), ([Char], [Char]), ([Char], [Char]), ([Char], [Char]), ([Char], [Char]))]
  mapColoring = [(("Alabama", a), ("Mississippi", m), ("Georgia", g), ("Tennessee", t), ("Florida", f)) |
      a <- colors, m <- colors, g <- colors, t <- colors, f <- colors,
      m /= t,
      m /= a,
      a /= t,
      a /= g,
      a /= f,
      g /= f,
      g /= t]
      where colors = ["red", "green", "blue"];
