## Monads
> Find a few monad tutorials.

http://www.cs.cornell.edu/~akhirsch/monads.html <br>
https://www.haskell.org/tutorial/monads.html <br>
https://dev.to/codenoodle/don-t-read-this-monad-tutorial-2cfc <br>
https://wiki.haskell.org/All_About_Monads

> Find a list of the monads in Haskell.

https://wiki.haskell.org/Monad

## Hash Table
> Write a function that looks up a hash table value that uses the Maybe monad. Write a hash that stores other hashes, several levels deep. Use a Maybe monad to retrieve an element several levels deep.

See `Day 3: HashTable.hs` for the solution.
