module Main where
  import Data.List

  main :: IO ()
  main = return ()

  halve :: [a] -> ([a], [a])
  halve xs = splitAt (div (length xs) 2) xs

  -- Sort
  mergeSort :: Ord a => [a] -> [a]
  mergeSort []     = []
  mergeSort [x]    = [x]
  mergeSort xs     = merge (mergeSort as) (mergeSort bs)
    where (as, bs) = halve xs

  merge :: Ord a => [a] -> [a] -> [a]
  merge [] l          = l
  merge l []          = l
  merge (x:xs) (y:ys)
    | x < y           = x : merge xs (y:ys)
    | otherwise       = y : merge (x:xs) ys

  -- Sort By
  mergeSortBy :: (a -> a -> Ordering) -> [a] -> [a]
  mergeSortBy f []  = []
  mergeSortBy f [x] = [x]
  mergeSortBy f xs  = mergeBy f (mergeSortBy f as) (mergeSortBy f bs)
    where (as, bs)  = halve xs

  mergeBy :: (a -> a -> Ordering) -> [a] -> [a] -> [a]
  mergeBy _ [] l          = l
  mergeBy _ l []          = l
  mergeBy f (x:xs) (y:ys)
    | f x y == LT         = x : mergeBy f xs (y:ys)
    | otherwise           = y : mergeBy f (x:xs) ys

  -- Number to String (There must be an easier way)
  toString :: Show a => a -> String
  toString n = "$" ++ (insertComma i) ++ d
    where (i, d)  = case elemIndex '.' (show n) of
                    Just a  -> splitAt a (show n)
                    Nothing -> (show n, [])

  insertComma, insertComma' :: String -> String
  insertComma xs = reverse $ insertComma' $ reverse xs

  insertComma' xs
    | length xs > 3 = (take 3 xs) ++  "," ++ (insertComma' (drop 3 xs))
    | otherwise     = xs

  -- Lazy sequences
  third, fifth :: Int -> [Int]
  third = nth 3

  fifth = nth 5

  nth, eighth :: Int -> Int -> [Int]
  eighth x y = zipWith (+) (third x) (fifth y)

  nth n x = [x] ++ (nth n $ x + n)

  -- Partial functions
  halveNumber :: Fractional a => a -> a
  halveNumber = flip (/) 2.0

  newLine :: String -> String
  newLine = flip (++) "\n"

  -- Greatest common denominator
  gcd :: Int -> Int -> Int
  gcd x y = gcd' x y ns
    where range = (min x y) - 1
          ns    = reverse [1..range]

  gcd' :: Int -> Int -> [Int] -> Int
  gcd' x y (n:ns)
    | mod x n == 0 && mod y n == 0 = n
    | otherwise                    = gcd' x y ns

  -- Lazy prime
  primes :: [Int]
  primes = primes' 3

  primes' :: Int -> [Int]
  primes' n
    | isPrime n = [n] ++ primes' (n + 1)
    | otherwise = primes' (n + 1)

  isPrime :: Int -> Bool
  isPrime n = and [not $ mod n x == 0 | x <- [2..(n-1)]]

  -- Lines
  breakAt :: Int -> String -> [[Char]]
  breakAt n s            = res ++ [word]
    where ws             = words s
          l              = zip ws (map length ws)
          (_, word, res) = foldl (flip (f n)) (0, "", []) l

  f :: Int -> ([Char], Int) -> (Int, [Char], [[Char]]) -> (Int, [Char], [[Char]])
  f n (word, len) (lineLen, currentLine, lines)
    | len + lineLen + 1 < n = (lineLen + len + 1, currentLine ++ " " ++ word, lines)
    | otherwise             = (len, word, lines ++ [currentLine])

  lineNumbers :: [[Char]] -> [[Char]]
  lineNumbers lines = zipWith (++) numbers lines
    where numbers = map (\x -> (show x) ++ ": ") [1..]

  loremIpsum :: [Char]
  loremIpsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ultrices tempor interdum. Aenean venenatis felis a nunc luctus, in efficitur leo congue. Vivamus quam ex, ornare ac odio id, ultrices dapibus velit. Ut sodales bibendum libero. Quisque nulla lectus, fermentum nec cursus ut, pharetra ac justo. Duis ut faucibus tortor, vitae dapibus tortor. Maecenas varius pulvinar velit et imperdiet. Donec accumsan magna sit amet nulla gravida condimentum. Quisque tortor justo, consectetur non pretium a, fringilla vitae justo."
