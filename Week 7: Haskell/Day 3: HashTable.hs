module Main where
  import Prelude hiding (lookup)

  main :: IO ()
  main = return ()

  -- This implementation is not a HashTable but a list of key value pairs.
  -- The performance is O(n) instead of the O(1) from a typical HashTable.
  type HashTable key value = [(key, value)]

  lookup :: Eq key => HashTable key value -> key -> Maybe value
  lookup [] _           = Nothing
  lookup ((k,v):xs) key
    | key == k          = Just v
    | otherwise         = lookup xs key

  insert :: Eq key => HashTable key value -> key -> value -> HashTable key value
  insert l key value = (key, value) : l

  inputData :: HashTable String Int
  inputData = [("Key1", 10), ("Key2", 20), ("Key3", 30)]  
