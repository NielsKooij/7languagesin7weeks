## List functions
> Find functions that you can use on lists, strings, or tuples

List: https://hackage.haskell.org/package/base-4.14.0.0/docs/Data-List.html
<br>
String: http://hackage.haskell.org/package/base-4.14.0.0/docs/Data-String.html
<br>
Tuple: http://hackage.haskell.org/package/base-4.14.0.0/docs/Data-Tuple.html

> Find a way to sort lists

https://hoogle.haskell.org/?hoogle=Ord%20a%20%3D%3E%20%5Ba%5D%20-%3E%20%5Ba%5D

## Sort
> Write a sort that takes a list and returns a sorted list.

> Write a sort that takes a list and a function that compares its two arguments and then returns a sorted list.

See `Day 2.hs` for the solutions. Use as following:

```
*Main> let list = [8,3,7,9,2,5,7,3,7,2,8,9,5,3]
*Main> mergeSort list
[2,2,3,3,3,5,5,7,7,7,8,8,9,9]
*Main> mergeSortBy (compare) list
[2,2,3,3,3,5,5,7,7,7,8,8,9,9]
```

## Number to String
> Write a Haskell function to convert a string to a number. The string should be in the form of $2,345,678.99 and can possibly have leading zeros.

See `Day 2.hs` for the solution. Use as following:

```
*Main> toString 2345678.99
"$2,345,678.99"
```

## Lazy
> Write a function that takes an argument x and returns a lazy sequence that has every third number, starting with x. Then, write a function that includes every fifth number, beginning with y. Combine these functions through composition to return every eight number, beginning with x + y.

See `Day 2.hs` for the solution. Use as following:

```
*Main> take 10 $ third 7
[7,10,13,16,19,22,25,28,31,34]
*Main> take 10 $ fifth 7
[7,12,17,22,27,32,37,42,47,52]
*Main> take 10 $ eighth 7 7
[14,22,30,38,46,54,62,70,78,86]
```

## Partial function
> Use a partially applied function to define a function that will return half of a number and another that will append `\n` to the end of any string.

See `Day 2.hs` for the solution. Use as following:

```
*Main> halveNumber 10
5.0
*Main> newLine "A string"
"A string\n"
```

## Greatest common denominator
> Write a function to determine the greatest common denominator of two integers.

See `Day 2.hs` for the solution. Use as following:

```
*Main> gcd 8 12
4
*Main> gcd 6574 9688
346
```

## Lazy primes
> Create a lazy sequence of prime numbers.

See `Day 2.hs` for the solution. Use as following:

```
take 10 $ primes
[3,5,7,11,13,17,19,23,29,31]
```

## Lines
> Break a long string into individual lines at proper word boundaries.

> Add line numbers to the previous exercise.


See `Day 2.hs` for the solutions. Use as following:

```
*Main> breakAt 100 loremIpsum
[" Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ultrices tempor interdum. Aenean",
"venenatis felis a nunc luctus, in efficitur leo congue. Vivamus quam ex, ornare ac odio id,",
"ultrices dapibus velit. Ut sodales bibendum libero. Quisque nulla lectus, fermentum nec cursus ut,",
"pharetra ac justo. Duis ut faucibus tortor, vitae dapibus tortor. Maecenas varius pulvinar velit et",
"imperdiet. Donec accumsan magna sit amet nulla gravida condimentum. Quisque tortor justo,",
"consectetur non pretium a, fringilla vitae justo."]

*Main> lineNumbers it
["1:  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ultrices tempor interdum. Aenean",
"2: venenatis felis a nunc luctus, in efficitur leo congue. Vivamus quam ex, ornare ac odio id,",
"3: ultrices dapibus velit. Ut sodales bibendum libero. Quisque nulla lectus, fermentum nec cursus ut,",
"4: pharetra ac justo. Duis ut faucibus tortor, vitae dapibus tortor. Maecenas varius pulvinar velit et",
"5: imperdiet. Donec accumsan magna sit amet nulla gravida condimentum. Quisque tortor justo,",
"6: consectetur non pretium a, fringilla vitae justo."]
```
