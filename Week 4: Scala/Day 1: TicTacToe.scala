import scala.io.StdIn.readInt

class Board {

  val combinations = Array(
    (0,1,2), (3,4,5), (6,7,8),
    (0,3,6), (1,4,7), (2,5,8),
    (0,4,8), (2,4,6))
  var board = Array(0,0,0,0,0,0,0,0,0)

  //Return the winner of the board or -1 when there is no winner or 0 when the board is full
  def getWinner(): Int = {
    for(c <- combinations) {
      if(board(c._1) != 0 &&
        board(c._1) == board(c._2) &&
        board(c._2) == board(c._3)) {
        return board(c._1)
      }
    }

    for(e <- board) {
      if(board(e) == 0) return -1
    }
    return 0
  }

  def doMove(player: Int, move: Int): Unit = {
    if(!((player == 1 || player == 2) && move >= 0 && move < 9 && board(move) == 0)) throw new RuntimeException("Invalid move")

    board(move) = player
  }

  def prettyPrint(): Unit = {
    println(board(0) + "|" + board(1) + "|" + board(2))
    println("-----")
    println(board(3) + "|" + board(4) + "|" + board(5))
    println("-----")
    println(board(6) + "|" + board(7) + "|" + board(8))
  }
}

class Game {
  var turn = 1
  val board = new Board()

  def start(): Unit = {
    var winner = board.getWinner()

    do {
      println("Current board: ")
      board.prettyPrint()
      println("")

      print("(Player " + turn + "): Make a move from 0-8: ")
      val move = readInt()
      board.doMove(turn, move)
      if(turn == 1) turn = 2 else turn = 1

      winner = board.getWinner()
    } while(winner == -1);

    if(winner == 0) {
      println("The game ended in a Tie!")
    }else {
      println("Player " + winner + " won the game!")
    }
  }
}

val game = new Game()
game.start()
