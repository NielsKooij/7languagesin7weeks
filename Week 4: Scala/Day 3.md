## Count links
> Take the sizer application and add a message to count the number of links on the page.

> Make the sizer follow the links on a given page, and load them as well. For example, a sizer for "google.com" would compute the size for Google and all the pages it links to.

Because some of the examples from the book are outdated I had to find my own examples for this problem. I understand how the actor system works but I do not fully understand how to design a good program with it yet. I hope to learn some more about this in the next week. The implementation I wrote works but is a bit messy and probably doesn't fully capture the strengths of this actor system.
<br><br>
See `day3/src/main/scala/nielskooij/SizerProgram.scala` for the solutions.
