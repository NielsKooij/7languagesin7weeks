## Scala API
> Find the Scala API.

https://www.scala-lang.org/api/current/index.html

## Scala and Java
> Find a comparison between Scala and Java

https://www.guru99.com/scala-vs-java.html

## val vs var
> Find a discussion of val vs var

https://www.java67.com/2017/05/difference-between-var-val-and-def-in-Scala.html

## TicTacToe
> Let two players play tic-tac-toe.

See `Day 1: TicTacToe.scala` for the solution.
