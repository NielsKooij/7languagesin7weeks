## Files
> Find a discussion on how to use scala files.

https://www.tutorialspoint.com/scala/scala_file_io.htm

## Closures vs code blocks
> What makes a closure different from a code block?

https://alvinalexander.com/scala/how-to-use-closures-in-scala-fp-examples/

## foldleft
> Use foldLeft to compute the total size of a list of strings.

```
scala> val strings = List("A", "list", "of", "strings")
strings: List[String] = List(A, list, of, strings)

scala> strings.foldLeft(0)((sum, value) => sum + value.size)
res1: Int = 14
```

## Censor
> Write a Censor trait with a method that will replace the curse words `Shoot` and `Darn` with `Pucky` and `Beans` alternatives. Use a map to store the curse words and their alternatives.

> Load the curse words and alternatives from a file.

See `Day 2: CurseWords.scala` for the solution.
