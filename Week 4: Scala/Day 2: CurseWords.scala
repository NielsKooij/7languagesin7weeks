import scala.io.Source

val curseWords = Map("Shoot" -> "Pucky", "Darn" -> "Beans")

def censor(text: String, curseWords: Map[String, String]): String =
  curseWords.foldLeft(text)((res, curse) => res.replaceAll(curse._1, curse._2))

val text = "Lorem ipsum dolor sit Shoot, consectetur adipiscing elit. Morbi sagittis sagittis Darn, ut venenatis arcu efficitur et. Darn potenti. Maecenas vehicula leo et Shoot sollicitudin. Maecenas tristique quam id libero."
println(censor(text, curseWords))

def cursesFromFile(fileName: String) : Map[String, String] =
  Source.fromFile(fileName).getLines()
    .map(line => line.split(","))
    .foldLeft(Map[String, String]())((map, parts) => map + (parts(0) -> parts(1)))

val fileCurses  = cursesFromFile("Cursewords.csv")
println(censor(text, fileCurses))