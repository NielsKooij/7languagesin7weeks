package nielskooij

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.pattern.ask

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.io.Source
import scala.util.{Failure, Success}

object SizerProgram {

  def main(args: Array[String]) : Unit = {
    val urls = List("http://www.amazon.com/",
      "http://www.twitter.com",
      "http://www.google.com/",
      "http://www.cnn.com/")

    val system = ActorSystem("SizerSystem")
    val pageSizerActor = system.actorOf(Props[PageActor], name = "PageActor")

    val sizer = new Sizer(pageSizerActor)

    println("Sequential run: ")
    timeMethod { sizer.sizeOfPageSequentially(urls) }

    println("Concurrent run: ")
    timeMethod { sizer.sizeOfPageConcurrently(urls) }

    Thread.sleep(10000)
    system.terminate()
  }

  def timeMethod[R](block: => R): R = {
    val start = System.nanoTime()
    val result = block
    val end = System.nanoTime()
    println(s"Method took ${(end - start) / 1000000000.0} seconds.")
    result
  }

}

object PageLoader {
  def loadPage(url: String): String = Source.fromURL(url).mkString
}

object SizerProtocol {
  case class Page(url: String, page: String)
  case class Pages(urls: Map[String, String])
}

class PageActor extends Actor {
  import nielskooij.SizerProtocol._

  def receive = {
    case Page(url, page) =>
      sender ! page
    case Pages(urls) =>
      sender ! urls
  }
}

class Sizer(val pageSizeActor : ActorRef) {
  import nielskooij.SizerProtocol._
  import akka.util.Timeout
  import scala.language.postfixOps
  import scala.concurrent.ExecutionContext.Implicits.global

  implicit val duration = Timeout(20 seconds)

  val linkRegex = "<a\\s+(?:[^>]*?\\s+)?href=([\"'])(.*?)\\1".r

  def sizeOfPageSequentially(urls : List[String]): Unit =
    urls.foreach(url => {
      val page = PageLoader.loadPage(url)
      val links = linkRegex
        .findAllIn(page)
        .map(tag => tag.substring(tag.indexOf("href=\"") + "href=\"".length, tag.lastIndexOf("\"")))
        .filter(s => s.startsWith("http"))

      val traversedLinks = (links map (t => t -> PageLoader.loadPage(t)) toMap)
      val traversedLinksSize = traversedLinks.foldLeft(0)((total, entry) => total + entry._2.length)

      println(s"Size of $url: ${page.length}")
      println(s"\tNumber of links: ${traversedLinks.size}")
      println(s"\tSize of links: $traversedLinksSize")
    })

  def sizeOfPageConcurrently(urls: List[String]): Unit =
    urls.foreach(url => {
      (pageSizeActor ? Page(url, PageLoader.loadPage(url))).mapTo[String].onComplete {
        case Success(res) => {
          val links = linkRegex
            .findAllIn(res)
            .map(tag => tag.substring(tag.indexOf("href=\"") + "href=\"".length, tag.lastIndexOf("\"")))
            .filter(s => s.startsWith("http"))

          val linkFutures = links.map(link => (pageSizeActor ? Page(link, PageLoader.loadPage(link))).mapTo[String])
          val linksFuture = Future.sequence(linkFutures)

          linksFuture.onComplete {
            case Success(traversedLinks) => {
              val traversedLinksSize = traversedLinks.foldLeft(0)((total, elem) => total + elem.length)

              println(s"Size of $url: ${res.length}")
              println(s"\tNumber of links: ${traversedLinks.length}")
              println(s"\tSize of links: $traversedLinksSize")
            }
            case Failure(ex) => ex.printStackTrace()
          }
        }
        case Failure(ex) => ex.printStackTrace()
      }
    })
}
